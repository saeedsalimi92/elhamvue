<?php





function callOpenAI($prompt)
{
    $apiKey = "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";
    $url = "https://api.openai.com/v1/chat/completions";

    // داده‌های مورد نیاز برای درخواست
    $data = [
        "model" => "gpt-4", // یا "gpt-4" اگر دسترسی دارید
        "messages" => [
            ["role" => "system", "content" => "You are a helpful assistant."],
            ["role" => "user", "content" => $prompt]
        ]
    ];

    // ارسال درخواست به API
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
        "Content-Type: application/json",
        "Authorization: Bearer $apiKey"
    ]);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

    $response = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE); // وضعیت HTTP
    curl_close($ch);

    // پردازش پاسخ API
    $result = json_decode($response, true);

    if ($httpcode !== 200) {

        // نمایش کد و پیام خطا
        return "Error: HTTP $httpcode - " . ($result['error']['message'] ?? "Unknown error");
    }

    if (isset($result['choices'][0]['message']['content'])) {
        return $result['choices'][0]['message']['content'];
    } else {
        return "Error: No response received from OpenAI.";
    }
}

// اجرای کد از طریق CLI
if (php_sapi_name() === "cli") {
    if ($argc > 1) {
        $prompt = implode(" ", array_slice($argv, 1));
        echo callOpenAI($prompt) . PHP_EOL;
    } else {
        echo "Usage: php openai.php [your prompt here]" . PHP_EOL;
    }
}
