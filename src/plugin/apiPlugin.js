const ApiPlugin = {
    install(Vue, axios) {
        Vue.prototype.$api = {
            
            'updateWallpaperByID': function (id, input) {
                return new Promise((resolve, reject) => {
                    axios.patch('wallpapers/' + id, input).then(response => {
                        return resolve(response)
                    }).catch(error => {
                        return reject(error)
                    })
                })
            },

            'getPost': function (id) {
                return new Promise((resolve, reject) => {
                    axios.get('blog/item/get.php?id=' + id).then(response => {
                        return resolve(response)
                    }).catch(error => {
                        return reject(error)
                    })
                })
            },
            'getTimes': function () {
                return new Promise((resolve, reject) => {
                    axios.get('time-slots').then(response => {
                        return resolve(response)
                    }).catch(error => {
                        return reject(error)
                    })
                })
            },
            'availableTimes': function (date) {
                return new Promise((resolve, reject) => {
                    axios.get(`available-time-slots?date=${date}`).then(response => {
                        return resolve(response)
                    }).catch(error => {
                        return reject(error)
                    })
                })
            },
            'getPosts': function (page) {
                return new Promise((resolve, reject) => {
                    if(!page){
                        page = 1
                    }
                    axios.get('blog/item/getAll.php?page=' + page).then(response => {
                        return resolve(response)
                    }).catch(error => {
                        return reject(error)
                    })
                })
            },
            'checkPhone': function (phone) {
                return new Promise((resolve, reject) => {
                    let params = new URLSearchParams()
                    params.append('phone' , phone)
                    axios.post("user/login/sendPhone.php",params).then(response => {
                        return resolve(response)
                    }).catch(error => {
                        return reject(error)
                    })
                })
            }, 'login': function (phone,password,otp,type) {
                return new Promise((resolve, reject) => {
                    let params = new URLSearchParams()
                    type === 'password' ? params.append('password', password) : params.append('otp', otp)
                    params.append('login_type', type)
                    params.append('phone' , phone)
                    axios.post("user/login/login.php",params).then(response => {
                        return resolve(response)
                    }).catch(error => {
                        return reject(error)
                    })
                })
            },
            'updateUser': function (id,password) {
                return new Promise((resolve, reject) => {
                    let params = new URLSearchParams()
                    params.append('password', password)
                    params.append('register_type', '2')
                    params.append('id', id)
                    axios.post("user/login/update.php",params).then(response => {
                        return resolve(response)
                    }).catch(error => {
                        return reject(error)
                    })
                })
            },'logout': function (phone) {
                return new Promise((resolve, reject) => {
                    let params = new URLSearchParams()
                    params.append('phone', phone)
                    axios.post("user/login/logout.php",params).then(response => {
                        return resolve(response)
                    }).catch(error => {
                        return reject(error)
                    })
                })
            },
            'sendCode': function (phone) {
                return new Promise((resolve, reject) => {
                    let params = new URLSearchParams()
                    params.append('phone' , phone)
                    axios.post("user/login/sendOTP.php",params).then(response => {
                        return resolve(response)
                    }).catch(error => {
                        return reject(error)
                    })
                })
            },

            'getServices': function (page) {
                return new Promise((resolve, reject) => {
                    if(!page){
                        page = 1
                    }
                    axios.get('service/item/getAll.php?page=' + page).then(response => {
                        return resolve(response)
                    }).catch(error => {
                        return reject(error)
                    })
                })
            },
            'getService': function (id) {
                return new Promise((resolve, reject) => {
                    axios.get('service/item/get.php?id=' + id).then(response => {
                        return resolve(response)
                    }).catch(error => {
                        return reject(error)
                    })
                })
            },
            'deletePost': function (id,link) {
                let params = new URLSearchParams()
                params.append('id', id)
                return new Promise((resolve, reject) => {
                    axios.post(link , params).then(response => {
                        return resolve(response)
                    }).catch(error => {
                        return reject(error)
                    })
                })
            },
            'support': function (input) {
                let params = new URLSearchParams()
                params.append('name', input.name)
                params.append('email', input.email)
                params.append('message', input.message)
                params.append('subject', input.select)
                params.append('phone', input.phone)
                return new Promise((resolve, reject) => {
                    axios.post('users/support.php' , params).then(response => {
                        return resolve(response)
                    }).catch(error => {
                        return reject(error)
                    })
                })
            },
            'deleteImage': function (id,image,link) {
                let params = new URLSearchParams()
                params.append('id', id)
                params.append('image', image)
                return new Promise((resolve, reject) => {
                    axios.post(link , params).then(response => {
                        return resolve(response)
                    }).catch(error => {
                        return reject(error)
                    })
                })
            },
            'createPost': function (obj, link , id) {
                return new Promise((resolve, reject) => {
                    let params = new URLSearchParams()
                    let errorField=
                        {
                            response:{
                                data:{
                                    type:'field',
                                    message:'پر کردن فیلد ها الزامی است'
                                }
                            }
                        }
                    if (!obj.title || !obj.description ||!obj.tags || !obj.select)  return reject(errorField)
                    if(id){
                        params.append('id', id)
                    }
                    params.append('title', obj.title)
                    params.append('description', obj.description)
                    params.append('authors', 'admin')
                    if(obj.select === 'فعال'){
                        obj.select = 1
                    }else{
                        obj.select = 0
                    }
                    params.append('status', obj.select)
                    params.append('tags', obj.tags)
                    axios.post(link, params).then(response => {
                        return resolve(response)
                    }).catch(error => {
                        return reject(error)
                    })
                })
            },
            'getInformation': function () {
                return new Promise((resolve, reject) => {
                    axios.get('data/get.php').then(response => {
                        return resolve(response)
                    }).catch(error => {
                        return reject(error)
                    })
                })
            },
            'getInfo': function () {
                return new Promise((resolve, reject) => {
                    axios.get('data/getInfo.php').then(response => {
                        return resolve(response)
                    }).catch(error => {
                        return reject(error)
                    })
                })
            },
            'createPhoto': function (obj) {

                let params = new URLSearchParams()

                params.append('image', obj)
                return new Promise((resolve, reject) => {
                    axios.post('post/upload.php',params,{
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    }).then(response => {
                        return resolve(response)
                    }).catch(error => {
                        return reject(error)
                    })
                })
            },


            /**
             *
             * @param {number}id
             * @returns {Promise<any>}
             */
            'deleteWallpaperByID': function (id) {
                return new Promise((resolve, reject) => {
                    axios.delete('wallpapers/' + id).then(response => {
                        return resolve(response)
                    }).catch(error => {
                        return reject(error)
                    })
                })
            }
        }
    }
}

export default ApiPlugin
