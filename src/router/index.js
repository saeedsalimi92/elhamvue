import Vue from 'vue'
import store from '../store'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import About from '../views/About.vue'
import Service from '../views/ServiceDetail.vue'
import News from '../views/NewsDetail.vue'
import Services from '../views/Service.vue'
import Gallery from '../views/Gallery.vue'
import Blog from '../views/News.vue'


import Appointment from '../views/Appointment.vue'

Vue.use(VueRouter)

const routes = [

  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: About
  },{
    path: '/blog',
    name: 'Blog',
    component: Blog
  },{
    path: '/service',
    name: 'Services',
    component: Services
  },{
    path: '/blog/:id',
    name: 'News',
    component: News
  },{
    path: '/service/:id',
    name: 'Service',
    component: Service
  },{
    path: '/gallery',
    name: 'Gallery',
    component: Gallery
  },{
    path: '/appointment',
    name: 'Appointment',
    component: Appointment
  },
  {
    path: '*',
    redirect : Home
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})
router.beforeEach((to, from, next) => {
    window.scrollTo(0, 0)
    if (to.name === 'About'){

        store.commit('activeFooter' , false)

    }else {
        store.commit('activeFooter' , true)
    }
    store.commit('activeType' , false)
    next()

})


export default router
