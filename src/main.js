import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './style/main.styl'

import ApiPlugin from './plugin/apiPlugin'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import VueLazyLoad from 'vue-lazyload'





import "swiper/swiper.min.css"
import Axios from 'axios'
import 'vuetify/dist/vuetify.min.css'
import vuetify from '@/plugin/vuetify' // path to vuetify export

const momentJalaali = require('moment-jalaali')
const BASE_URL = 'http://localhost:8000/api/'
var axiosClient = Axios.create({
  baseURL: BASE_URL,
  'Authorization':localStorage.getItem('token_type') + ' ' + localStorage.getItem('token')
})
router.beforeEach((to, from, next) => {
  document.title = to.meta.title
  next()
})
import VueMeta from 'vue-meta'


Vue.use(VueMeta, {
  // optional pluginOptions
  refreshOnceOnNavigation: true
})
Vue.use(ApiPlugin, axiosClient)
Vue.use(VueLazyLoad)
Vue.config.productionTip = false
Vue.use(VueAwesomeSwiper)

Vue.filter('toJalaaliFull', function (value) {
  return momentJalaali(value).format('jYYYY/jM/jD HH:mm:ss')
})
Vue.filter('toJalaaliDate', function (value) {
  return momentJalaali(value).format('jYYYY/jM/jD')
})
Vue.filter('toJalaaliTime', function (value) {
  return momentJalaali(value).format('HH:mm:ss')
})
Vue.filter('toJalaaliTime2', function (value) {
  const time = `2002/02/08 ${value}`
  return momentJalaali(time).format('HH:mm')
})
Vue.filter('toHrDigits', function (Num) {
  Num += ''
  Num = Num.replace(/,/g, '')
  let x = Num.split('.')
  let x1 = x[0]
  let x2 = x.length > 1 ? '.' + x[1] : ''
  let rgx = /(\d+)(\d{3})/
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, '$1' + ',' + '$2')
  }
  return x1 + x2
})

new Vue({
  router,
  vuetify,
  store,
  render: h => h(App)
}).$mount('#app')
