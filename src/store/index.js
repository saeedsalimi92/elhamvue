import Vue from 'vue'
import Vuex from 'vuex'

const img1 = require('../assets/img/image/01.jpeg')
const img2 = require('../assets/img/image/02.jpeg')
const img3 = require('../assets/img/image/03.jpeg')
const img4 = require('../assets/img/image/04.jpeg')


Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        address:'https://goo.gl/maps/e7buEA77rQfcWiCg7',
        information_site:{
          title:'مرکز مشاوره پرواز',
          admin:'ُسعید سلیمی',
          description:'دکتر الهام سلیمی',
          phone:'021 55 92 63 88',
          phone_link:'tel:+982155926388',
          mobile:'0938 001 72 42',
          mobile_link:'tel:+989380017242',
          email:'info@elhamsalimi.ir',
          email_link:'info@elhamsalimi.ir',
          address:'شهرری، خیابان فداییان اسلام، نرسیده به میدان بسیج مسطضعفین (سه راه ورامین)، پلاک 230، طبقه پنجم، واحد 20',
          time:' شنبه تا پنج شنبه از ساعت 14 الی 20 ',
        },
        footer: true,
        type: false,
        new: 1,
        information: {
            address: '',
            address_link: '',
            email: '',
            fax: '',
            id: '',
            instagram: '',
            mobile: '',
            phone: '',
            telegram: '',
            facebook: '',
            aparat: '',
            youtube: '',
            title: '',
            en_title: '',
            subtitle: '',
            en_subtitle: '',
            en_description: '',
            description: '',
            whatsapp: '',
        },
        images: [
            {
                thumb: img1,
                src: img1,
                caption: 'image1'
            },
            {
                thumb: img2,
                src: img2,
                caption: 'image2'
            },
            {
                thumb: img3,
                src: img3,
                caption: 'image3'
            }, {
                thumb: img4,
                src: img4,
                caption: 'image4'
            },
        ],

    },
    mutations: {
        activeFooter(state, payload) {
            state.footer = payload
        },
        activeType(state, payload) {
            state.type = payload
            state.new += 1
        },
        setInfo(state, payload) {
            state.information = payload
            if(state.information.mobile){
                const regex = /^(\d{1})(\d*)/gm;
                const m = regex.exec(state.information.mobile)
                state.information.mobile_link = 'tel:+98' + m[2]
            }
            if(state.information.phone){
                state.information.phone_link = 'tel:+9821' + state.information.phone
            }
            if (state.information.whatsapp) {
                const regex = /^(\d{1})(\d*)/gm;
                const m = regex.exec(state.information.whatsapp)
                state.information.whatsapp_link = 'https://api.whatsapp.com/send/?phone=98' + m[2]
            }
            if(state.information.email){
                state.information.email_link = 'mailto:' + state.information.email
            }


            state.information.time = ' شنبه تا پنج شنبه از ساعت 14 الی 20 '
        },

    },
    getters: {

        getFooter: state => {
            return state.footer
        },
        getInformation: state => {
            return state.information_site
        },
        getInfo: state => {
            return state.information
        },
        getType: state => {
            return state.type
        },



    },

    actions: {
    },
    modules: {}
})
